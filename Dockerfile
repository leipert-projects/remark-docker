FROM node:10-alpine

WORKDIR /app

ADD app /app

RUN apk add --no-cache --update bash \
    && yarn \
    && yarn cache clean \
    && ln -s /app/remark.sh /usr/local/bin/remark
