#!/bin/bash
# Unofficial strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

if [ ! -d '/app/doc' ]; then
    echo "Please make sure that you have a directory with markdown files at /app/doc"
    exit 1
fi

echo "Running markdown linter"

/app/node_modules/.bin/remark '/app/doc/**/*.md' --no-stdout --quiet --frail

echo "Successfully run all linted files"